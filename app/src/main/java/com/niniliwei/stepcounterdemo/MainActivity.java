package com.niniliwei.stepcounterdemo;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements Handler.Callback, View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private TextView stepsView;

    private Messenger messenger;
    private Messenger mGetReplyMessenger = new Messenger(new Handler(this));

//    StepCounterService stepCounterService;
    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
//            StepCounterService.StepBinder binder = (StepCounterService.StepBinder) service;
//            stepCounterService = binder.getService();

            messenger = new Messenger(service);
            try {
                Message msg = Message.obtain(null, Constant.MSG_FROM_CLIENT);
                msg.replyTo = mGetReplyMessenger;
                messenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stepsView = (TextView) findViewById(R.id.steps);
        findViewById(R.id.button_refresh).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, StepCounterService.class);
        bindService(intent, conn, Context.BIND_AUTO_CREATE);
//        startService(intent);
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case Constant.MSG_FROM_SERVER:
                Log.d(TAG, "handleMessage: msg from server");
                // 更新界面上的步数
                stepsView.setText(String.valueOf(msg.getData().getInt("step")));
//                delayHandler.sendEmptyMessageDelayed(Constant.REQUEST_SERVER, TIME_INTERVAL);
                break;
            case Constant.REQUEST_SERVER:
                Log.d(TAG, "handleMessage: msg request server");
                try {
                    Message msg1 = Message.obtain(null, Constant.MSG_FROM_CLIENT);
                    msg1.replyTo = mGetReplyMessenger;
                    messenger.send(msg1);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        try {
            Message msg = Message.obtain(null, Constant.REQUEST_SERVER);
            mGetReplyMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
//        stepsView.setText(String.valueOf(stepCounterService.getTodaySteps()));
    }
}
