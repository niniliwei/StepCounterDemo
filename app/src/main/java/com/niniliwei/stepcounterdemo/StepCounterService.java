package com.niniliwei.stepcounterdemo;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by liwei on 2016/11/7 10:01
 * Email: liwei@uama.com.cn
 * Description:
 */

public class StepCounterService extends Service implements Handler.Callback, SensorEventListener{

    private static final String TAG = StepCounterService.class.getSimpleName();
    private static final String TODAY_FIRST_STEPS = "today_first_steps";
    private static final String TODAY_DATE = "today_date";
    private Messenger messenger = new Messenger(new Handler(this));

//    private int counterSteps = 0;
    private int yesterdayCounterSteps = 0;
    private int todaySteps = 0;
    private int todayFirstCounterSteps = 0;

    private final IBinder mBinder = new StepBinder();

    SharedPreferences sp;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
//        return mBinder;
        return messenger.getBinder();
    }

    public class StepBinder extends Binder {

        StepCounterService getService() {
            return StepCounterService.this;
        }
    }

    public int getTodaySteps() {
        return todaySteps;
    }

    @Override
    public boolean handleMessage(Message msg) {
        if (msg.what == Constant.MSG_FROM_CLIENT) {
            Log.d(TAG, "handleMessage() called with: msg from client");
            try {
                Messenger messenger = msg.replyTo;
                Message replyMsg = Message.obtain(null, Constant.MSG_FROM_SERVER);
                Bundle bundle = new Bundle();
                bundle.putInt("step", todaySteps);
                replyMsg.setData(bundle);
                messenger.send(replyMsg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // 如果不支持计步功能，关闭服务
        if (!isKitkatWithStepSensor()) stopSelf();
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        sp = getSharedPreferences("steps", 0);
        if (isKitkatWithStepSensor()) {
            registerEventListener();
        }
    }

    private void registerEventListener() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * 判断是否具有计步功能
     * @return true 如果系统在4.4（含）以上并且具有相应传感器
     */
    private boolean isKitkatWithStepSensor() {
        // BEGIN_INCLUDE(iskitkatsensor)
        // Require at least Android KitKat
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        // Check that the device supports the step counter and detector sensors
        PackageManager packageManager = getPackageManager();
        return currentApiVersion >= android.os.Build.VERSION_CODES.KITKAT
                && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER);
        // END_INCLUDE(iskitkatsensor)
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int sensorValue = (int) event.values[0];
        String today_date = sp.getString("today_date", "");
//        if (!today_date.equals(getTodayDate())) {
//            yesterdayCounterSteps = sp.getInt(today_date, 0);
//            sp.edit().putString("today_date", getTodayDate()).apply();
//        }
//        today_date = getTodayDate();
//        sp.edit().putInt(today_date, (int) event.values[0]).apply();
//
//        if (yesterdayCounterSteps < 1) {
//            yesterdayCounterSteps = (int) event.values[0];
//        }
//
//        todaySteps = (int) event.values[0] - yesterdayCounterSteps;
//        Log.d(TAG, "onSensorChanged() called with: todaySteps = [" + todaySteps + "], yesterdayCounterSteps = [" + yesterdayCounterSteps + "]");

        if (!today_date.equals(getTodayDate())) {
            // 如果日期变了，保存当前日期的第一个传感器数据，用于计算差值
            todayFirstCounterSteps = sensorValue;
            sp.edit().putInt(TODAY_FIRST_STEPS, todayFirstCounterSteps).apply();
            today_date = getTodayDate();
            sp.edit().putString(TODAY_DATE, today_date).apply();
        }

        // 如果今天的第一步为0，从 sp 中获取
        if (todayFirstCounterSteps < 1) {
            todayFirstCounterSteps = sp.getInt(TODAY_FIRST_STEPS, 0);
        }

        if (sensorValue < todayFirstCounterSteps) {
            todayFirstCounterSteps = sensorValue;
            sp.edit().putInt(TODAY_FIRST_STEPS, todayFirstCounterSteps).apply();
        }

        todaySteps = sensorValue - todayFirstCounterSteps;

        Log.d(TAG, "onSensorChanged() called with: todaySteps = [" + todaySteps + "], todayFirstCounterSteps = [" + todayFirstCounterSteps + "]");
    }

    private String getTodayDate() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
